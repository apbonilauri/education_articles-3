# XS (eXtreme Situation) analysis in learning environments: A mixed methods study within a reproducible research framework

Adelia Batilana
Elias Carvalho
Joao Vissoci
Ricardo Pietrobon

<!-- Joao, reproducible here means that we will insert the R code inside this document --> 


## Abstract
<!-- will write at the end --> 

## Introduction


## Methods
<!-- discussion about integration between qualitative and quantitative -->
<!-- flowchart with overall study design -->
<!-- QCA package?? http://goo.gl/nTsp5 -->


### Qualitative component
<!-- check qualitative literature for name of interventional qualitative -->
<!-- add qualitative headings from template -->


### Quantitative component


#### Adaptive assessment
<!-- characteristics and timing -->
<!-- explore R package for qualitative analysis -->


#### Graphical exploratory analysis
<!-- select best graphics from ggplot2, googlevis, and others -->
<!-- explore logic methodology from sociology -->


### Reproducible research protocol

As partially described in previous sections, we followed a reproducible research protocol along the lines previously described by our group [(Vissoci, 2013)](). Briefly, all data sets, scripts, templates, software and workflows generated under this project were deposited under the public repositories [Github]() and [figshare](). For access to the specific data for this project please refer to 
<!-- please add reference once available -->

reproducible research - data and .rmd, shiny, css, js, irt display from g+ repository, zotero library, concerto files

[Item analysis application](https://github.com/EconometricsBySimulation/2013-05-29-ShinyApp) by Francis Smart


<!-- rprior -->

http://trinker.github.io/qdap/

<!-- 1. nas mensagens de feedback, que atualmente sao muito boring, criar interrupcoes indo de coisas simples do tipo o Chuck entrando na conversa (Xande, o Chuck e o meu bulldoguinho), pessoas entrando na conversa, brigas e discussoes, pedindo opinioes pra outras pessoas, tropecando, deixando o computador "cair," fazendo de conta que esqueceu o computador ligado e fazendo outras coisas 2. pra comedia: checar coisas que estejam fazendo sucesso no youtube ou de pecas de teatro e que sejam faceis de reproduzir com pouca tecnologia 3. pra narrativa, seja misterio ou intriga: comecar a colocar algumas imagens mais chocantes, tipo sangue (acho que coloquei uma mas nao sei se o SIRI deletou), som de arma de fogo, videos com um monte de pessoas falando ao mesmo tempo (o ator no meio de nao atores) -->


## Results



## Discussion

### Primacy and summary

### Result 1
### Result 2
### Result 3
### Result 4
### Limitations

### Future




http://goo.gl/qUc1k codrops