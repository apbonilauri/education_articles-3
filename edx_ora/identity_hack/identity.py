# next steps: http://goo.gl/nC1HTn , better stemming http://goo.gl/M8NZ8k and better tokenization http://goo.gl/DxNcl5
# if these don't work, then might want to contact some specialists http://goo.gl/11IVm1

# %autoindent # toggles autoindent to off
# install below on terminal and then close terminal before you open ipython
# sudo pip install -U pyyaml nltk
# sudo pip install -U numpy

import nltk

identity = open('identity_data.txt').read()
type(identity)
print identity
tokens = nltk.word_tokenize(identity) #creates tokens which roughly corresponds to words in a list. notice that prior to this step we had a collection of characters rather than words, and so the software could not tell what a word was. need to try other tokenization methods as there are still issues with some of the words
words = [w.lower() for w in tokens]
type(words)
porter = nltk.PorterStemmer()
stem = [porter.stem(t) for t in words] # here will also need to adapt to portuguese http://goo.gl/M8NZ8k . also check with http://goo.gl/LI5KLG
wnl = nltk.WordNetLemmatizer()
lemma = [wnl.lemmatize(t) for t in stem]

vocab = sorted(set(lemma))
type(vocab)
vocab_samp = vocab[100:200] 
print(vocab_samp)
nltk.pos_tag(vocab_samp) # can't tell the syntatic role of each word (part of speech) as it is using the English version of wordnet - see http://goo.gl/BAkJjs

# frequency distribution
for char in vocab_samp: print char
fdist = nltk.FreqDist(ch.lower() for ch in vocab_samp if ch.isalpha())
fdist.keys()
fdist.plot() # all of them occur once. has to find a way to first sort all of them and then plot the first, say, 100

# LSA using python



